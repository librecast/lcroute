/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2022 Brett Sheffield <bacs@librecast.net> */

#include "test_lcroute.h"
#include "../src/peer.h"

int main(void)
{
	lc_relay_t *relay;
	lc_peer_t *peer[3];
	int rc, peerid;

	test_name("peer_add() / peer_get() / peer_del()");

	relay = lc_relay_init("eth0");
	//lc_relay_start(relay);

	peer[0] = peer_get(relay, 0);
	test_assert(peer[0] == NULL, "peer_get() returns NULL on invalid id");

	/* add peer */
	peerid = peer_add(relay, AF_INET, "192.0.2.1");
	test_assert(peerid == 0, "peer_add() returns 0 (%i)", peerid);
	peer[peerid] = peer_get(relay, peerid);
	test_assert(peer[peerid] != NULL, "peer_get() returns peer object");
	test_assert(peerid == peer_id(peer[peerid]), "peer_id() returns correct id");

	/* add another peer */
	peerid = peer_add(relay, AF_INET, "192.0.2.2");
	test_assert(peerid == 1, "peer_add() returns 1 (%i)", peerid);
	peer[peerid] = peer_get(relay, peerid);
	test_assert(peer[peerid] != NULL, "peer_get() returns peer object");
	test_log("peerid = %i\n", peerid);
	test_assert(peerid == peer_id(peer[peerid]), "peer_id() returns correct id");

	/* fetch first peer again */
	peer[0] = peer_get(relay, 0);
	test_assert(peer[0] != NULL, "peer_get() returns peer object");
	test_assert(0 == peer_id(peer[0]), "peer_id() returns correct id");

	/* add same peer IP again (error) */
	rc = peer_add(relay, AF_INET, "192.0.2.1");
	test_assert(rc == -1, "peer_add() returns -1 on error (%i)", rc);
	test_assert(errno == EEXIST, "duplicate, errno == EEXIST (IPv4)");

	/* add IPv6 peer */
	rc = peer_add(relay, AF_INET6, "2001:0DB8::");
	test_assert(rc == 2, "peer_add() - IPv6 peer (%i)", rc);

	/* add duplicate */
	rc = peer_add(relay, AF_INET6, "2001:0DB8::");
	test_assert(rc == -1, "peer_add() - IPv6 peer (%i) - DUPLICATE", rc);
	test_assert(errno == EEXIST, "duplicate, errno == EEXIST (IPv6)");

	lc_relay_free(relay);

	return fails;
}
