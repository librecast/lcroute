/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2022 Brett Sheffield <bacs@librecast.net> */

#include "test_lcroute.h"
#include "../src/peer.h"
#include <unistd.h>

int main(void)
{
	struct lc_stp_s stp[2] = {0};
	int rc;

	test_name("lc_stp_cmp()");

	rc = lc_stp_cmp(&stp[0], &stp[1]);
	test_assert(rc == 0, "empty stp are equal");

	stp[1].root = 1;
	rc = lc_stp_cmp(&stp[0], &stp[1]);
	test_assert(rc < 0, "roots differ: stp1 < stp2, return negative (%i)", rc);

	stp[0].root = 2;
	rc = lc_stp_cmp(&stp[0], &stp[1]);
	test_assert(rc > 0, "roots differ: stp1 > stp2, return postitive (%i)", rc);

	stp[0].root = 2; stp[1].root = 2;
	stp[0].cost = 2; stp[1].cost = 2;
	stp[0].port = 2; stp[1].port = 2;
	rc = lc_stp_cmp(&stp[0], &stp[1]);
	test_assert(rc == 0, "stp (root, cost, port) all equal, return zero (%i)", rc);

	stp[0].root = 2; stp[1].root = 2;
	stp[0].cost = 1; stp[1].cost = 2;
	stp[0].port = 2; stp[1].port = 2;
	rc = lc_stp_cmp(&stp[0], &stp[1]);
	test_assert(rc < 0, "stp root equal, stp1.cost lower, return negative (%i)", rc);

	stp[0].root = 2; stp[1].root = 2;
	stp[0].cost = 2; stp[1].cost = 1;
	stp[0].port = 2; stp[1].port = 2;
	rc = lc_stp_cmp(&stp[0], &stp[1]);
	test_assert(rc > 0, "stp root equal, stp1.cost higher, return positive (%i)", rc);

#if 0
	stp[0].root = 2; stp[1].root = 2;
	stp[0].cost = 2; stp[1].cost = 2;
	stp[0].port = 1; stp[1].port = 2;
	rc = lc_stp_cmp(&stp[0], &stp[1]);
	test_assert(rc < 0, "stp root,cost equal, stp1.port lower, return negative (%i)", rc);

	stp[0].root = 2; stp[1].root = 2;
	stp[0].cost = 2; stp[1].cost = 2;
	stp[0].port = 2; stp[1].port = 1;
	rc = lc_stp_cmp(&stp[0], &stp[1]);
	test_assert(rc > 0, "stp root,cost equal, stp1.port higher, return positive (%i)", rc);
#endif
	return fails;
}
