/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2022 Brett Sheffield <bacs@librecast.net> */

#include "test_lcroute.h"
#include "../src/peer.h"
#include <arpa/inet.h>
#include <librecast.h>
#include <librecast/if.h>
#include <linux/sockios.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/ioctl.h>
#include <unistd.h>

/*
 * Run peering/tunnel protocol tests.
 *
 * Create two threads, each with a tap,
 * connected by a bridge.
 *
 * Set IPs on each tap, and start a relay in each
 * thread, adding a peer of the other.
 *
 *    +------+ tap +--------+ tap +------+
 *    | tid0 |-----| bridge |-----| tid1 |
 *    +------+     +--------+     +------+
 */

char bridgename[] = "forth";
sem_t sem_ip;

void *thread_relay(void *arg)
{
	lc_relay_t *relay;
	lc_ctx_t *lctx;
	struct ifreq ifr = {0};
	struct sockaddr_in saddr = {0};
	char ifname[IFNAMSIZ] = {0};
	char myip[INET_ADDRSTRLEN] = {0};
	char peerip[INET_ADDRSTRLEN] = {0};
	char *ptr;
	unsigned int ifx;
	int id = 1, fd, sock, peerid, rc;

	lctx = lc_ctx_new();

	/* try to grab semaphore to determine our id */
	if (sem_trywait(&sem_ip) == -1) id++;
	else {
		/* create bridge */
		rc = lc_bridge_add(lctx, bridgename);
		test_assert(rc == 0 || rc == EEXIST, "bridge created (%i)", rc);
		rc = lc_link_set(lctx, bridgename, 1);
		test_assert(rc == 0, "bridge up (%i)", rc);
	}
	fprintf(stderr, "id: %i\n", id);

	/* create tap and connect to bridge */
	fd = lc_tap_create(ifname);
	test_assert(fd > 0, "fd: %i", fd);
	ifx = if_nametoindex(ifname);
	test_assert(ifx > 0, "ifx: %u", ifx);
	lc_link_set(lctx, ifname, 1);
	rc = lc_bridge_addif(lctx, bridgename, ifname);

	/* set IP to 192.0.2.id */
	strncpy(ifr.ifr_name, ifname, sizeof ifname);
	saddr.sin_family = AF_INET;
	saddr.sin_addr.s_addr = id << 24;
	saddr.sin_addr.s_addr |= 2 << 16;
	saddr.sin_addr.s_addr |= 192;
	ptr = (char *)&ifr + offsetof(struct ifreq, ifr_addr);
	memcpy(ptr, &saddr, sizeof(struct sockaddr));
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	ioctl(sock, SIOCSIFADDR, &ifr);
	close(sock);

	relay = lc_relay_init(ifname);

	/* add peer of other IP */
	inet_ntop(AF_INET, &saddr.sin_addr.s_addr, myip, sizeof myip);
	saddr.sin_addr.s_addr = ((id == 1) ? 2 : 1) << 24;
	saddr.sin_addr.s_addr |= 2 << 16;
	saddr.sin_addr.s_addr |= 192;
	inet_ntop(AF_INET, &saddr.sin_addr.s_addr, peerip, sizeof peerip);
	peerid = peer_add(relay, AF_INET, peerip);
	test_assert(peerid >= 0, "thread %i, peerid: %i, ip: %s, peer: %s", id, peerid, myip, peerip);

	lc_relay_start(relay);

	/* ready to run tests */

	sleep(1); /* wait for hellos to be exchanged */

	/* test for hello packet. We'll do this by checking the lastseen
	 * timer rather than poking at internals or directly checking packets. */
	struct timespec *ts;
	ts = lc_peer_lastseen(relay, peerid);
	test_assert(ts != NULL, "got peer last seen time");
	test_assert(ts->tv_sec, "peer last seen at %i", ts->tv_sec);

#if 0
	if (id == 1) {
		lc_link_set(lctx, bridgename, 0);
		lc_bridge_del(lctx, bridgename);
	}
#endif
	lc_relay_free(relay);
	close(fd);
	lc_ctx_free(lctx);

	return arg;
}

int main(void)
{
	pthread_t tid[2];

	test_name("lc_relay_start()");

	sem_init(&sem_ip, 0, 1);
	for (int i = 0; i < 2; i++) {
		pthread_create(&tid[i], NULL, &thread_relay, NULL);
	}

	for (int i = 0; i < 2; i++) pthread_join(tid[i], NULL);
	sem_destroy(&sem_ip);

	return fails;
}
