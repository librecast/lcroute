/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2021 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include <librecast.h>
#include <limits.h>
#include <net/if.h>
#include <sys/types.h>
#include <ifaddrs.h>

/* find an invalid interface index */
unsigned int get_invalid_ifx(void);

/* find an interface that supports multicast */
unsigned get_multicast_if(void);

