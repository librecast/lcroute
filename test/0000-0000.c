/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2022 Brett Sheffield <bacs@librecast.net> */

#include "test_lcroute.h"
#include "../src/router.h"
#include <librecast/if.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

enum {
	IF_OUT,
	IF_IN,
	IF_COUNT
};

static sem_t sem_msg;

void *callback_msg(void *arg)
{
	lc_socket_t *sock = (lc_socket_t *)arg;
	char buf[BUFSIZ];
	ssize_t byt;
	byt = lc_socket_recv(sock, buf, sizeof buf, 0);
	test_assert(byt > 0, "lc_socket_recv: %s", strerror(errno));
	test_log("callback_msg - got msg of %zi bytes\n", byt);
	sem_post(&sem_msg);
	return arg;
}

int main(void)
{
	lc_ctx_t *lctx;
	lc_socket_t *sock[IF_COUNT];
	lc_channel_t *chan[IF_COUNT];
	pthread_t tid;
	struct timespec ts = {0};
	char ifname[IF_COUNT][IFNAMSIZ] = {0};
	int fd[IF_COUNT];

	return test_skip("router_start() / router_stop()");

	router_start();

	lctx = lc_ctx_new();

	/* create random channel + copy */
	chan[IF_OUT] = lc_channel_random(lctx);
	test_assert(chan[IF_OUT] != NULL, "chan[OUT] created");
	chan[IF_IN] = lc_channel_copy(lctx, chan[IF_OUT]);
	test_assert(chan[IF_IN] != NULL, "chan[IN] created");

	for (int i = 0; i < IF_COUNT; i++) {
		/* create tap interface */
		fd[i] = lc_tap_create(ifname[i]);
		test_assert(fd[i] > 0, "tap[%i] created", i);
		lc_link_set(lctx, ifname[i], 1);

		/* create and bind socket to respective interface */
		sock[i] = lc_socket_new(lctx);
		test_assert(sock[i] != NULL, "sock[%i] created", i);
		lc_socket_bind(sock[i], if_nametoindex(ifname[i]));

		/* bind channel to socket */
		lc_channel_bind(sock[i], chan[i]);
	}

	lc_channel_join(chan[IF_IN]);
	sleep(1);

	/* start listen thread */
	pthread_create(&tid, NULL, &callback_msg, sock[IF_IN]);
	pthread_detach(tid);
	//lc_socket_listen(sock[IF_IN], &callback_msg, NULL);

	sleep(1);

	/* send OUT, check received on IN */
	sleep(1);
	lc_channel_send(chan[IF_OUT], "hi", 2, 0);
	clock_gettime(CLOCK_REALTIME, &ts);
	ts.tv_sec += 2;
	test_assert(sem_timedwait(&sem_msg, &ts) == 0, "got msg");

	/* clean up */
	//lc_socket_listen_cancel(sock[IF_IN]);
	router_stop();
	for (int i = 0; i < IF_COUNT; i++) close(fd[i]);
	lc_ctx_free(lctx);

	return fails;
}
