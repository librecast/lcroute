/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2022 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include <librecast.h>
#include <limits.h>
#include <net/if.h>
#include <sys/types.h>
#include <ifaddrs.h>

unsigned int get_invalid_ifx(void)
{
	char ifname[IF_NAMESIZE];
	for (unsigned int ifx = 1; ifx < UINT_MAX; ifx++) {
		if (!if_indextoname(ifx, ifname)) return ifx;
	}
	return 0;
}

/* find an interface that supports multicast */
unsigned get_multicast_if(void)
{
	unsigned ifidx = 0;
	struct ifaddrs *ifa = NULL, *ifap = NULL;
	test_assert(getifaddrs(&ifa) != -1, "getifaddrs(): %s", strerror(errno));
	for (ifap = ifa; ifap; ifap = ifap->ifa_next) {
		if (!(ifap->ifa_flags & IFF_MULTICAST)) continue;
		if (ifap->ifa_addr == NULL) continue;
		if (ifap->ifa_addr->sa_family != AF_INET6) continue;
		ifidx = if_nametoindex(ifap->ifa_name);
		test_log("found multicast interface %s\n", ifap->ifa_name);
		break;
	}
	freeifaddrs(ifa);
	return ifidx;
}
