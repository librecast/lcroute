/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2022 Brett Sheffield <bacs@librecast.net> */

#include "test_lcroute.h"
#include "../src/peer_pvt.h"
#include <unistd.h>

#define PEERS 2

int main(void)
{
	lc_relay_t *relay;
	lc_peer_t *peer[PEERS] = {0};
	struct lc_stp_s stp = {0};
	struct timespec now = {0};
	char ip[INET_ADDRSTRLEN] = {0};
	int peerid[PEERS];
	uint64_t age;
	uint64_t ourid = 42;
	int rc;

	test_name("STP state tests");

	relay = lc_relay_init("fake0");
	test_assert(relay != NULL, "lc_relay_init()");

	for (int i = 0; i < PEERS; i++) {
		snprintf(ip, sizeof ip, "192.168.0.%i", i+1);
		peerid[i] = peer_add(relay, AF_INET, ip);
		test_assert(peerid[i] >= 0, "peer_add() %s (peerid=%i)", ip, peerid[i]);
		peer[i] = peer_get(relay, peerid[i]);
		test_assert(peer[i] != NULL, "peer_get()");
	}

	/* set our router ID */
	lc_relay_id_set(relay, ourid);
	test_assert(relay->id == ourid, "router id is set to %" PRIu64, relay->id);

	/* we have no msgs from peers yet, so we must be the root */
	test_assert(relay->id == relay->stp.root, "we are root: id = %" PRIu64, relay->stp.root);
	test_assert(relay->stp.cost == 0, "if we are root, cost == 0 (%u)", relay->stp.cost);
	test_assert(!(peer[0]->flags & PEER_UP), "peer not up (PEER_UP)");
	test_assert(!(peer[0]->flags & PEER_ROOT), "peer not root (PEER_ROOT)");

	stp.root = 99;
	stp.cost = 1;

	rc = peer_stp_msg(relay, peer[0], &stp);
	test_assert(rc == 0, "peer_stp_msg() worse root => no change (%i)", rc);
	test_assert(peer[0]->flags & PEER_UP, "peer up (PEER_UP)");
	test_assert(!(peer[0]->flags & PEER_ROOT), "peer not root (PEER_ROOT)");
	//test_assert(peer[0]->flags & PEER_DR, "we are DR for link(PEER_DR)");

	/* better root, but expired message => no change */
	stp.root = 1;
	stp.t = RELAY_PEER_EXPIRES + 1;
	rc = peer_stp_msg(relay, peer[0], &stp);
	test_assert(rc == 0, "peer_stp_msg() better root (expired) => no change (%i)", rc);
	test_assert(!memcmp(&stp, &peer[0]->stp, sizeof(struct lc_stp_s)),
			"peer msg updated");

	/* lower root => root updated */
	stp.root = 41;
	stp.t = 0;
	rc = peer_stp_msg(relay, peer[0], &stp);
	test_assert(rc == -1, "peer_stp_msg() root update (%i)", rc);
	test_assert(stp.root == peer[0]->stp.root, "peer msg (root)updated");
	test_assert(stp.cost == peer[0]->stp.cost, "peer msg (cost)updated");
	test_assert(peer[0]->stp.t > 0, "peer msg (t)updated");
	test_assert(relay->stp.root == stp.root, "relay state (root) updated");
	test_assert(relay->stp.cost == stp.cost + 1, "relay state (cost) updated");
	test_assert(peer[0]->seen.tv_sec > 0, "peer seen updated");
	test_assert(peer[0]->flags & PEER_UP, "PEER_UP");
	test_assert(peer[0]->flags & PEER_ROOT, "PEER_ROOT");
	test_assert(!(peer[0]->flags & PEER_DR), "we are no longer DR for link(PEER_DR)");

	stp.t++;
	rc = peer_stp_msg(relay, peer[0], &stp);
	test_assert(rc == 0, "peer_stp_msg() same root => no change (%i)", rc);
	test_assert(stp.root == peer[0]->stp.root, "peer msg updated (root)");
	test_assert(stp.cost == peer[0]->stp.cost, "peer msg updated (cost)");
	test_assert(relay->stp.t > 0, "root age updated");

	/* expire peer, reassume root role */
	peer[0]->seen.tv_sec = 0;
	peer_check(relay, peer[0]);
	test_assert(!(peer[0]->flags & PEER_UP), "peer not up (PEER_UP)");
	test_assert(!(peer[0]->flags & PEER_ROOT), "peer not root (PEER_ROOT)");
	test_assert(relay->id == relay->stp.root, "we are root: id = %" PRIu64, relay->stp.root);
	test_assert(relay->stp.cost == 0, "if we are root, cost == 0 (%u)", relay->stp.cost);

	/* bring peer back up => peer resumes root */
	stp.root = 41, stp.cost = 0, stp.t = 14;
	clock_gettime(CLOCK_REALTIME, &now);
	rc = peer_stp_msg(relay, peer[0], &stp);
	test_assert(rc == -1, "peer_stp_msg() peer back up => resume root (%i)", rc);
	test_assert(peer[0]->flags & PEER_UP, "peer up (PEER_UP)");
	test_assert(peer[0]->flags & PEER_ROOT, "peer is root (PEER_ROOT)");
	//test_assert(!(peer[0]->flags & PEER_DR), "we are not DR for link(PEER_DR)");
	age = (uint64_t)now.tv_sec - stp.t;
	test_assert(peer[0]->stp.t == age,
			"peer root msg age set %" PRIu64 " (should be %" PRIu64 ")",
			peer[0]->stp.t, age);

	/* send another msg, ensure age updated */
	stp.root = 41, stp.cost = 0, stp.t = 5;
	clock_gettime(CLOCK_REALTIME, &now);
	rc = peer_stp_msg(relay, peer[0], &stp);
	age = (uint64_t)now.tv_sec - stp.t;
	test_assert(peer[0]->stp.t == age,
			"peer root msg age updated %" PRIu64 " (should be %" PRIu64 ")",
			peer[0]->stp.t, age);

	/* expire peer, reassume root role (again) */
	peer[0]->seen.tv_sec = 0;
	peer_check(relay, peer[0]);
	test_assert(!(peer[0]->flags & PEER_UP), "peer not up (PEER_UP)");
	test_assert(!(peer[0]->flags & PEER_ROOT), "peer not root (PEER_ROOT)");
	test_assert(relay->id == relay->stp.root, "we are root: id = %" PRIu64, relay->stp.root);
	test_assert(relay->stp.cost == 0, "if we are root, cost == 0 (%u)", relay->stp.cost);

	/* bring peer back up, not root */
	stp.root = 99, stp.cost = 0, stp.t = 0;
	rc = peer_stp_msg(relay, peer[0], &stp);
	test_assert(rc == 0, "peer_stp_msg() peer back up, worse root => no change (%i)", rc);
	test_assert(peer[0]->flags & PEER_UP, "peer up (PEER_UP)");
	test_assert(!(peer[0]->flags & PEER_ROOT), "peer not root (PEER_ROOT)");
	//test_assert(peer[0]->flags & PEER_DR, "we are DR for link(PEER_DR)");

	/* bring up second peer with better root path */
	stp.root = 40, stp.cost = 2, stp.t = 0;
	rc = peer_stp_msg(relay, peer[1], &stp);
	test_assert(relay->stp.root == stp.root, "relay state (new root) updated");
	test_assert(relay->stp.cost == stp.cost + 1, "relay state (cost) updated");
	test_assert(peer[1]->seen.tv_sec > 0, "peer seen updated");
	test_assert(peer[1]->flags & PEER_UP, "peer up (PEER_UP)");
	test_assert(peer[1]->flags & PEER_ROOT, "new peer is root (PEER_ROOT)");
	//test_assert(!(peer[1]->flags & PEER_DR), "we are not DR for link(PEER_DR)");

	/* first peer has higher cost to same root than us */
	stp.root = 40, stp.cost = 2, stp.t = 0;
	rc = peer_stp_msg(relay, peer[0], &stp);
	test_assert(rc == 0, "peer_stp_msg() peer has higher cost to same root => no change (%i)", rc);
	test_assert(!(peer[0]->flags & PEER_ROOT), "peer not root (PEER_ROOT)");
	//test_assert(peer[0]->flags & PEER_DR, "we are DR for link(PEER_DR)");

	/* first peer has lower cost to same root than us */
	stp.root = 40, stp.cost = 1, stp.t = 0;
	rc = peer_stp_msg(relay, peer[0], &stp);
	test_assert(rc == -1, "peer_stp_msg() peer lower cost to same root => updated (%i)", rc);
	test_assert(peer[0]->flags & PEER_ROOT, "peer[0] is root (PEER_ROOT)");
	test_assert(!(peer[1]->flags & PEER_ROOT), "peer[1] is not root (PEER_ROOT)");

	/* expire peer[0], peer[1] takes over */
	peer[0]->seen.tv_sec = 0;
	peer_check(relay, peer[0]);
	test_assert(peer[1]->flags & PEER_ROOT, "peer resumes root (PEER_ROOT)");
	//test_assert(!(peer[1]->flags & PEER_DR), "we are not DR for link(PEER_DR)");
	test_assert(peer[1]->flags & PEER_UP, "peer up (PEER_UP)");

	/* TODO write test to repeat live behaviour
	 * - create peers
	 * - set up state
	 * - shut down (42)
	 * - sleep 21s
	 * - check state
	 */

	lc_relay_free(relay);

	return fails;
}
