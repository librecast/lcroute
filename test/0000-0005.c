/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2022 Brett Sheffield <bacs@librecast.net> */

#include "test_lcroute.h"
#include "../src/peer_pvt.h"
#include <unistd.h>

#define PEERS 3

int main(void)
{
	lc_relay_t *relay;
	lc_peer_t *peer[PEERS] = {0};
	struct lc_stp_s stp = {0};
	struct timespec now = {0};
	char ip[INET_ADDRSTRLEN] = {0};
	int peerid[PEERS];
	uint64_t ourid = 99;
	int rc;

	test_name("STP root crash resolution");

	relay = lc_relay_init("fake0");
	test_assert(relay != NULL, "lc_relay_init()");

	for (int i = 0; i < PEERS; i++) {
		snprintf(ip, sizeof ip, "192.168.0.%i", i+1);
		peerid[i] = peer_add(relay, AF_INET, ip);
		test_assert(peerid[i] >= 0, "peer_add() %s (peerid=%i)", ip, peerid[i]);
		peer[i] = peer_get(relay, peerid[i]);
		test_assert(peer[i] != NULL, "peer_get()");
	}

	/* set our router ID */
	lc_relay_id_set(relay, ourid);
	test_assert(relay->id == ourid, "router id is set to %" PRIu64, relay->id);

	/* test to repeat live behaviour
	 * - create peers
	 * - set up state
	 * - shut down (42)
	 * - sleep 21s
	 * - check state
	 *
	 *   self   - id = 99
	 *   fr [0] - id = 98
	 *   de [1] - id = 101
	 *   us [2] - id = 42
	 */

	stp.root = 98; stp.cost = 0; stp.t = 0;
	rc = peer_stp_msg(relay, peer[0], &stp);
	test_assert(rc == -1, "FR becomes root");

	for (int i = 0; i < PEERS; i++) peer_check(relay, peer[i]);

	stp.root = 101; stp.cost = 0; stp.t = 0;
	rc = peer_stp_msg(relay, peer[1], &stp);
	test_assert(rc == 0, "DE joins");

	for (int i = 0; i < PEERS; i++) peer_check(relay, peer[i]);

	stp.root = 42; stp.cost = 0; stp.t = 0;
	rc = peer_stp_msg(relay, peer[2], &stp);
	test_assert(rc == -1, "US becomes root");

	for (int i = 0; i < PEERS; i++) peer_check(relay, peer[i]);

	stp.root = 42; stp.cost = 1; stp.t = 1;
	rc = peer_stp_msg(relay, peer[1], &stp);
	test_assert(rc == 0, "DE offers route to US");

	for (int i = 0; i < PEERS; i++) peer_check(relay, peer[i]);

	stp.root = 42; stp.cost = 2; stp.t = 2;
	rc = peer_stp_msg(relay, peer[0], &stp);
	test_assert(rc == 0, "FR offers route to US");

	for (int i = 0; i < PEERS; i++) peer_check(relay, peer[i]);

#if 0
	sleep(RELAY_PEER_EXPIRES + 1);
#else
	/* US (42) goes down */
	peer[2]->seen.tv_sec = 0;
	/* all peers have expired records */
	for (int i = 0; i < PEERS; i++) {
		peer[i]->stp.t = now.tv_sec - RELAY_PEER_EXPIRES - 1;
	}
#endif
	for (int i = 0; i < PEERS; i++) peer_check(relay, peer[i]);
	test_assert(!(peer[2]->flags & PEER_ROOT), "US not root (PEER_ROOT)");

	stp.root = 98; stp.cost = 0; stp.t = 0;
	rc = peer_stp_msg(relay, peer[0], &stp);
	test_assert(rc == -1, "FR offers itself as root");
	test_assert(peer[0]->flags & PEER_ROOT, "FR is new root (PEER_ROOT)");

	lc_relay_free(relay);

	return fails;
}
