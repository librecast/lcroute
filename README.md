# lcroute - Librecast IPv6 Multicast Router

`lcroute` is an IPV6 (only) multicast router.  It manages the kernel multicast
routing tables. It is being developed to support the Librecast Project's R&D
projects as part of the NGI Zero (Next Generation Internet) Programme.

`lcroute` is not fully RFC-compliant for PIM or MLDv2, and there is no intention
to ever support MLDv1, or any form of IPv4 multicast.  `lcroute` exists purely
so we can experiment with multicast routing as part of our development work.

## Status

In development.  Pre-alpha.

## Build & Install

Dependencies:
- librecast

`make`

`make install`

## Usage

`lcroute [-b bridge] [--rp] iface ... [iface]`

In `--rp` mode, the first interface will be used as a Rendezvous Point interface
such that multicast JOINs/PARTs are propogated towards that interface only.

When not in `--rp` mode, PIM JOINs and PARTs will be propogated out all
interfaces except the originating interface.  This doesn't play nicely with
routing loops, so be careful.

The `-b` option will create `bridge` as a virtual bridge if it does not exist,
and create and connect a tap interface to that bridge.

## Questions, Bug reports, Feature Requests

New issues can be raised at:

https://github.com/librestack/lcroute/issues

It's okay to raise an issue to ask a question.  You can also email or ask on
IRC.

<hr />

### IRC channel

`#librecast` on libera.chat

If you have a question, please be patient. An answer might take a few hours
depending on time zones and whether anyone on the team is available at that
moment. 

<hr />

## License

This work is dual-licensed under GPL 2.0 and GPL 3.0.

SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

<hr />

# Funding

<p class="bigbreak">
This project was funded through the <a href="https://nlnet.nl/discovery"> NGI0 Discovery </a> Fund, a fund established by NLnet with financial support from the European
Commission's <a href="https://ngi.eu">Next Generation Internet</a> programme, under the aegis of DG Communications Networks, Content and Technology under grant agreement No 825322. *Applications are still open, you can <a href="https://nlnet.nl/propose">apply today</a>*
</p>

  <a href="https://nlnet.nl/project/LibrecastLive/">
      <img width="250" src="https://nlnet.nl/logo/banner.png" alt="Logo NLnet: abstract logo of four people seen from above" class="logocenter" />
  </a>
  <a href="https://ngi.eu/">
      <img width="250" align="right" src="https://nlnet.nl/image/logos/NGI0_tag.png" alt="Logo NGI Zero: letterlogo shaped like a tag" class="logocenter" />
  </a>
</p>
