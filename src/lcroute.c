/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2017-2022 Brett Sheffield <bacs@librecast.net> */

#include "globals.h"
#include "router.h"
#include "io.h"
#include "log.h"
#include <stdio.h>
#include <unistd.h>

int process_args(int argc, char *argv[])
{
	for (int i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "-c")) {
			if (i+1 >= argc) {
				ERROR("-c requires filename");
				return -1;
			}
			FILE *f = fopen(argv[++i], "r");
			if (!f) {
				ERROR("fopen: %s", strerror(errno));
				return -1;
			}
			while (!io_prompt(stdout, f));
			fclose(f);
		}
	}
	return 0;
}

int main(int argc, char *argv[])
{
	if (process_args(argc, argv) == -1) return 1;
	gethostname(hostname, HOST_NAME_MAX);
	strtok(hostname, ".");
	router_start();
	io_banner(stdout);
	while (!io_prompt(stdout, stdin));
	router_stop();
	return 0;
}
