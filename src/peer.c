/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2017-2022 Brett Sheffield <brett@librecast.net> */

#include "peer_pvt.h"
#include "router.h"
#include <assert.h>
#include <net/ethernet.h>
#include <netpacket/packet.h>

static void *get_in_addr(struct sockaddr_storage *sa)
{
	if (sa->ss_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}
	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int peer_id(lc_peer_t *peer)
{
	if (!peer) return -1;
	return peer->id;
}

lc_peer_t *peer_get(lc_relay_t *relay, int peerid)
{
	for (lc_peer_t *peer = relay->peer; peer; peer = peer->next) {
		if (peer->id == peerid) return peer;
	}
	return NULL;
}

struct timespec *lc_peer_lastseen(lc_relay_t *relay, int peerid)
{
	lc_peer_t *peer = peer_get(relay, peerid);
	if (!peer) return NULL;
	return &peer->seen;
}

lc_peer_t *peer_get_byaddr(lc_relay_t *relay, int af, struct sockaddr_storage *saddr)
{
	size_t sz;
	if (relay) for (lc_peer_t *peer = relay->peer; peer; peer = peer->next) {
		if (peer->af == af) {
			sz = (af == AF_INET6) ? sizeof(struct in6_addr) : sizeof(struct in_addr);
			if (!memcmp(get_in_addr(&peer->saddr), get_in_addr(saddr), sz))
				return peer;
		}
	}
	return NULL;
}

static int bind_interface(int sock, const int af, const char *ifname)
{
	struct ifaddrs *ifaddr, *bind_ifa = NULL;
	const int opt = 1;
	int rc = 0;
	if (getifaddrs(&ifaddr)) {
		ERROR("%s(): getifaddrs: %s", __func__, strerror(errno));
		return -1;
	}
	for (struct ifaddrs *ifa = ifaddr; ifa; ifa = ifa->ifa_next) {
		if (strcmp(ifname, ifa->ifa_name)) continue;
		if (ifa->ifa_addr->sa_family != af) continue; /* IPv4 */
		bind_ifa = ifa;
		break;
	}
	if (!bind_ifa) {
		errno = ENONET;
		rc = -1;
		goto exit_err_0;
	}
	((struct sockaddr_in *)bind_ifa->ifa_addr)->sin_port = htons(RELAY_PORT_UDP);
	if ((setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))) == -1) {
		ERROR("%s(): setsockopt(SO_REUSEADDR): %s", __func__, strerror(errno));
	}
	if ((setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &opt, sizeof(opt))) == -1) {
		ERROR("%s(): setsockopt(SO_REUSEPORT): %s", __func__, strerror(errno));
	}
	if (bind(sock, bind_ifa->ifa_addr, sizeof(struct sockaddr_in)) == -1) {
		if (errno != EINVAL) {
			ERROR("%s(): bind: %s", __func__, strerror(errno));
			rc = -1;
		}
	}
exit_err_0:
	freeifaddrs(ifaddr);
	return rc;
}

static void *thread_peer(void *arg)
{
	lc_peer_t *peer = (lc_peer_t *)arg;
	lc_relay_t *rel = peer->relay;
	char buf[1500] = {0};
	char straddr[INET6_ADDRSTRLEN];
	struct ethhdr eth = {0};
	struct sockaddr_in *dst;
	struct sockaddr_ll src = {0};
	struct iovec iov_recv[2];
	struct iovec iov_send[3];
	struct msghdr msg = {0};
	struct sock_fprog bpf = {
		.len = bpf_ip6multilen,
		.filter = bpf_ip6multi,
	};
	struct lc_stp_s stp = { .type = PKT_DATA, };
	struct sockaddr_ll addr = {
		.sll_family = AF_PACKET,
		.sll_protocol = htons(ETH_P_ALL)
	};
	ssize_t byt;
	dst = (struct sockaddr_in *)&peer->saddr;
	inet_ntop(AF_INET, &dst->sin_addr, straddr, INET6_ADDRSTRLEN);
	DEBUG("starting peer thread [%s]", straddr);
	iov_recv[0].iov_base = &eth;
	iov_recv[0].iov_len = sizeof eth;
	iov_recv[1].iov_base = buf;
	iov_recv[1].iov_len = sizeof buf;

	iov_send[0].iov_base = &stp; /* when sending, insert an STP header */
	iov_send[0].iov_len = sizeof stp;
	iov_send[1].iov_base = &eth;
	iov_send[1].iov_len = sizeof eth;
	iov_send[2].iov_base = buf;
	if ((peer->if_tun.sock = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) == -1) {
		perror("socket(LAN)");
		return NULL;
	}
	addr.sll_ifindex = peer->if_tun.ifx;
	if (bind(peer->if_tun.sock, (struct sockaddr *)&addr, sizeof addr) == -1) {
		perror("bind(TUN)");
		return NULL;
	}
	/* filter anything that isn't IPv6 multicast */
	if (setsockopt(peer->if_tun.sock, SOL_SOCKET, SO_ATTACH_FILTER, &bpf, sizeof(bpf)) == -1) {
		ERROR("%s() unable to set BPF filter: %s", __func__, strerror(errno));
		return NULL;
	}
	while (1) {
		msg.msg_name = &src;
		msg.msg_namelen = sizeof(struct sockaddr_storage);
		msg.msg_iov = iov_recv;
		msg.msg_iovlen = sizeof iov_recv / sizeof iov_recv[0];
		byt = recvmsg(peer->if_tun.sock, &msg, 0);
		if (byt == -1) perror("recvmsg");
		else if (byt > 0) {
			char ifname[IFNAMSIZ];
			int sent = 0;
			if_indextoname(src.sll_ifindex, ifname);
			DEBUG("LAN: %zi bytes received on %s(%i)", byt, ifname, src.sll_ifindex);
			examine_headers(buf, NULL);

			/* encapsulate data, send to peer */
			if ((peer->flags & PEER_DR) || (peer->flags & PEER_ROOT)) {
				/* peer has DR or ROOT flag set, forward */
				msg.msg_name = dst;
				msg.msg_namelen = sizeof(struct sockaddr_in);
				iov_send[2].iov_len = byt - sizeof eth;
				msg.msg_iov = iov_send;
				msg.msg_iovlen = sizeof iov_send / sizeof iov_send[0];
				byt = sendmsg(rel->if_wan.sock, &msg, 0);
				if (byt == -1) {
					perror("sendto");
				}
				else if (byt > 0) {
					sent++;
					DEBUG("TUN: %zi bytes sent to peer %s", byt, straddr);
				}
			}
			else DEBUG("dropping packet - %s not a DR|ROOT peer", straddr);
		}
	}
	return arg;
}

int peer_add(lc_relay_t *relay, int af, const char *restrict addr)
{
	static int peer_next_id;
	struct sockaddr_storage saddr = {0};
	if (!relay) {
		errno = EINVAL;
		return -1;
	}
	lc_peer_t *peer;
	((struct sockaddr_in *)&saddr)->sin_family = af;
	((struct sockaddr_in *)&saddr)->sin_port = htons(RELAY_PORT_UDP);
	if (inet_pton(af, addr, get_in_addr(&saddr)) == -1) {
		perror("inet_pton");
		return -1;
	}
	peer = peer_get_byaddr(relay, af, &saddr);
	if (peer) {
		errno = EEXIST;
		return -1;
	}
	peer = calloc(1, sizeof(lc_peer_t));
	if (relay->peer) peer->next = relay->peer;
	relay->peer = peer;
	peer->relay = relay;
	peer->id = peer_next_id++;
	peer->af = af;
	memcpy(&peer->saddr, &saddr, sizeof(struct sockaddr_storage));
	peer->tapfd = lc_tap_create(peer->if_tun.ifname);
	peer->if_tun.ifx = if_nametoindex(peer->if_tun.ifname);
	lc_link_set(relay->ctx, peer->if_tun.ifname, 1);
	pthread_create(&peer->tid, NULL, &thread_peer, peer);
	return peer->id;
}

void peer_freelist(lc_peer_t *peerlist)
{
	for (lc_peer_t *p = peerlist, *q = NULL; p; q = p, p = p->next, free(q)) {
		pthread_cancel(p->tid);
		pthread_join(p->tid, NULL);
	}
}

lc_relay_t *lc_relay_init(const char *restrict ifname)
{
	lc_relay_t *relay = malloc(sizeof(lc_relay_t));
	if (!relay) return NULL;
	memset(relay, 0, sizeof(lc_relay_t));
	relay->ctx = lc_ctx_new();
	lc_getrandom(&relay->id, sizeof relay->id);
	relay->stp.root = relay->id;
	strcpy(relay->if_wan.ifname, ifname);
	return relay;
}

void peer_list(lc_relay_t *relay)
{
	char straddr[INET6_ADDRSTRLEN] = {0};
	struct timespec now = {0};
	uint64_t age;
	time_t s;
	clock_gettime(CLOCK_REALTIME, &now);
	age = (relay->id == relay->stp.root) ? 0 : (uint64_t)now.tv_sec - relay->stp.t;
	INFO("root = (%" PRIu64 ", %u, %u, age: %" PRIu64 "s)",
			relay->stp.root, relay->stp.cost, relay->stp.port, age);
	if (relay) for (lc_peer_t *peer = relay->peer; peer; peer = peer->next) {
		inet_ntop(peer->af, get_in_addr(&peer->saddr), straddr, sizeof straddr);
		fprintf(stderr, "peer: %s last seen: ", straddr);
		if (peer->seen.tv_sec) {
			s = now.tv_sec - peer->seen.tv_sec;
			fprintf(stderr, "%" PRIu64 "s", s);
		}
		else fprintf(stderr, "never");
		if (peer->flags & PEER_UP) fprintf(stderr, " UP");
		if (peer->flags & PEER_DR) fprintf(stderr, " DR");
		if (peer->flags & PEER_ROOT) fprintf(stderr, " ROOT");
		fprintf(stderr, " (%" PRIu64 ", %u, %u)",
				peer->stp.root, peer->stp.cost, peer->stp.port);
		fputc('\n', stderr);
	}
}

int lc_stp_cmp(const struct lc_stp_s *stp1, const struct lc_stp_s *stp2)
{
	if (stp1->root < stp2->root) return -1;
	if (stp1->root > stp2->root) return 1;
	if (stp1->cost < stp2->cost) return -1;
	if (stp1->cost > stp2->cost) return 1;
#if 0
	if (stp1->port < stp2->port) return -1;
	if (stp1->port > stp2->port) return 1;
#endif
	return 0;
}

static void peer_clear_root(lc_relay_t *relay, lc_peer_t *peer)
{
	/* clear root flags for all other peers */
	for (lc_peer_t *p = relay->peer; p; p = p->next) {
		if (p != peer && (p->flags & PEER_ROOT)) p->flags ^= PEER_ROOT;
	}
}

#if 0
void calculate_root(lc_relay_t *relay)
{
	/* just set ourselves as root - ignore old data */
	struct lc_stp_s self = { .root = relay->id };

	/* clear root and DR flags for all peers */
	for (lc_peer_t *peer = relay->peer; peer; peer = peer->next) {
		if (peer->flags & PEER_ROOT) peer->flags ^= PEER_ROOT;
		if (peer->flags & PEER_DR) peer->flags ^= PEER_DR;
	}
	memcpy(&relay->stp, &self, sizeof(struct lc_stp_s));
}
#else
void calculate_root(lc_relay_t *relay)
{
	struct lc_stp_s self = { .root = relay->id, .cost = 0 };
	lc_peer_t *root = NULL;
	struct timespec now = {0};
	uint64_t age;
	clock_gettime(CLOCK_REALTIME, &now);
	/* choose best peer candidate for root */
	for (lc_peer_t *peer = relay->peer; peer; peer = peer->next) {
		DEBUG("considering (%" PRIu64 ", %u, %u)", peer->stp.root, peer->stp.cost, peer->stp.port);
		if (!(peer->flags & PEER_UP)) {
			DEBUG("peer is down, skipping, flags = %i", peer->flags);
			continue;
		}
		age = (uint64_t)now.tv_sec - (uint64_t)peer->seen.tv_sec;
		if (age >= RELAY_PEER_EXPIRES) {
			DEBUG("peer expired, skipping, flags = %i", peer->flags);
			continue;
		}
		age = (uint64_t)now.tv_sec - peer->stp.t;
		if (age >= RELAY_PEER_EXPIRES) {
			DEBUG("peer root msg expired, skipping, flags = %i", peer->flags);
			continue;
		}
		if (!root) {
			root = peer;
			continue;
		}
		if (lc_stp_cmp(&root->stp, &peer->stp) > 0) {
			root = peer;
		}
	}
	/* did we find a better root than ourselves? */
	if (!root || lc_stp_cmp(&root->stp, &self) > 0) {
		memcpy(&relay->stp, &self, sizeof(struct lc_stp_s));
		DEBUG("we are root %" PRIu64 ", %u, %u, 0s",
				relay->stp.root, relay->stp.cost, relay->stp.port);
		age = 0;
	}
	else {
		DEBUG("root chosen");
		memcpy(&relay->stp, &root->stp, sizeof(struct lc_stp_s));
		relay->stp.cost++;
		peer_clear_root(relay, root);
		root->flags |= PEER_ROOT;
		age = (uint64_t)now.tv_sec - relay->stp.t;
	}
	DEBUG("chosen (%" PRIu64 ", %u, %u, %" PRIu64 "s)",
			relay->stp.root, relay->stp.cost, relay->stp.port, age);
}
#endif

void peer_check(lc_relay_t *relay, lc_peer_t *peer)
{
	char straddr[INET6_ADDRSTRLEN] = {0};
	struct timespec now = {0};
	if (clock_gettime(CLOCK_REALTIME, &now) == -1) {
		ERROR("clock_gettime: %s", strerror(errno));
		return;
	}
	if (peer->seen.tv_sec + RELAY_PEER_EXPIRES < now.tv_sec) {
		if (!(peer->flags & PEER_UP)) return;
		inet_ntop(peer->af, get_in_addr(&peer->saddr), straddr, sizeof straddr);
		DEBUG("peer %s expired", straddr);
		peer->flags ^= PEER_UP;
		if (peer->flags & PEER_ROOT) {
			/* stale peer was on root port, recalculate */
			peer->flags ^= PEER_ROOT;
			if (relay->id == relay->stp.root) return;
			calculate_root(relay);
		}
	}
	if (relay->id == relay->stp.root && peer->flags & PEER_ROOT) {
		peer->flags ^= PEER_ROOT; /* we are root, so clear flag */
	}
}

/* say hellooooo to everybody
 *    0                   1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |    type (8)   |    cost (8)   |          port (16)            |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                                                               |
 * +                          root ID (64)                         +
 * |                                                               |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                                                               |
 * +                          time(s) (64)                         +
 * |                                                               |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
static int peer_hello_all(lc_relay_t *relay)
{
	char strpeer[INET6_ADDRSTRLEN] = {0};
	//struct sockaddr_storage *dst;
	struct iovec iov[1];
	struct msghdr msg = {0};
	struct lc_stp_s stp = {0};
	struct timespec now = {0};
	uint64_t age;
	ssize_t byt;
	int sock;

	/* check if stored root msg has expired */
	clock_gettime(CLOCK_REALTIME, &now);
	age = (relay->id == relay->stp.root) ? 0 : (uint64_t)now.tv_sec - relay->stp.t;
	if (age >= RELAY_PEER_EXPIRES) {
		calculate_root(relay);
		return 0;
	}
	age = (relay->id == relay->stp.root) ? 0 : (uint64_t)now.tv_sec - relay->stp.t;

	/* set STP header */
	if (relay->id != relay->stp.root) {
		/* we are not root, set age */
		stp.t = (uint64_t)now.tv_sec - relay->stp.t;
	}

#ifdef DEBUG_STP
	DEBUG("sending hellos: (%" PRIu64 ", %u , %u, %" PRIu64 "s)",
		relay->stp.root, relay->stp.cost, relay->stp.port, stp.t);
#endif
	stp.t = htobe64(stp.t);
	stp.cost = relay->stp.cost;
	stp.root = htobe64(relay->stp.root);

	if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		ERROR("%s(): socket: %s", __func__, strerror(errno));
		return -1;
	}
	for (lc_peer_t *peer = relay->peer; peer; peer = peer->next) {
		peer_check(relay, peer);
		if (bind_interface(sock, peer->af, relay->if_wan.ifname) == -1)
			continue;
		if (peer->af == AF_INET) {
			struct sockaddr_in *dst = (struct sockaddr_in *)&peer->saddr;
			inet_ntop(AF_INET, &dst->sin_addr, strpeer, sizeof strpeer);
			//DEBUG("sending hello to peer %s", strpeer);
			msg.msg_name = dst;
			msg.msg_namelen = sizeof(struct sockaddr_in);
		}
		else {
			errno = ENOSYS;
			return -1;
		}
#ifdef DEBUG_STP
		DEBUG("sending hello to peer %s", strpeer);
#endif

		stp.port = htobe16(peer->if_tun.ifx);

		msg.msg_iov = iov;
		msg.msg_iovlen = sizeof iov / sizeof iov[0];
		iov[0].iov_len = sizeof stp;
		iov[0].iov_base = &stp;
		if ((byt = sendmsg(sock, &msg, 0)) == -1) {
			ERROR("%s(): sendmsg: %s", __func__, strerror(errno));
		}
	}
	close(sock);
	return 0;
}

static void *thread_timer(void *arg)
{
	lc_relay_t *relay = (lc_relay_t *)arg;
	while (1) {
		peer_hello_all(relay);
		sleep(RELAY_PEER_INTERVAL);
	}
	return arg;
}

void lc_relay_id_set(lc_relay_t *relay, uint64_t id)
{
	relay->id = id;
	if (id < relay->stp.root) {
		relay->stp.root = id;
		relay->stp.cost = 0;
	}
	peer_hello_all(relay);
}

uint64_t lc_relay_id_get(lc_relay_t *relay)
{
	return relay->id;
}

/* process peer STP message, return 0 if no change, -1 if root updated */
int peer_stp_msg(lc_relay_t *relay, lc_peer_t *peer, const struct lc_stp_s *msg)
{
	struct lc_stp_s stp = {0};
	struct timespec now = {0};
	uint64_t age;

	/* work out DR for link */
	if (lc_stp_cmp(&relay->stp, msg) < 0 && stp.port < msg->port)
		peer->flags |= PEER_DR;
	else if (peer->flags & PEER_DR)
		peer->flags ^= PEER_DR;

	clock_gettime(CLOCK_REALTIME, &now);
	memcpy(&peer->stp, msg, sizeof(struct lc_stp_s));
	memcpy(&peer->seen, &now, sizeof(struct timespec));
	if (msg->t > RELAY_PEER_EXPIRES) return 0;
	memcpy(&stp, msg, sizeof(struct lc_stp_s));
	stp.cost++;
	peer->flags |= PEER_UP;
	stp.t = (uint64_t)now.tv_sec - peer->stp.t;
	peer->stp.t = stp.t;
	age = (relay->id == relay->stp.root) ? 0 : (uint64_t)now.tv_sec - relay->stp.t;
	if (age >= RELAY_PEER_EXPIRES) {
		calculate_root(relay);
		return 0;
	}
	if (peer->flags & PEER_ROOT) {
		relay->stp.t = stp.t;
	}
	if (lc_stp_cmp(&relay->stp, &stp) <= 0) return 0;
	memcpy(&relay->stp, &stp, sizeof(struct lc_stp_s));
	peer_clear_root(relay, peer);
	peer->flags |= PEER_ROOT;
	DEBUG("STP: new root (%" PRIu64 ", %u, %u, %" PRIu64 "s)",
			stp.root, stp.cost, stp.port, age);
	return -1;
}

static void process_stp(lc_relay_t *relay, struct sockaddr_storage *src, struct lc_stp_s *stp)
{
	char straddr[INET6_ADDRSTRLEN] = {0};
	struct timespec now = {0};
	stp->root = be64toh(stp->root);
	stp->port = be16toh(stp->port);
	stp->t = be64toh(stp->t);
	inet_ntop(src->ss_family, get_in_addr(src), straddr, INET6_ADDRSTRLEN);
#ifdef DEBUG_STP
	DEBUG("[%s] STP (%" PRIu64 ", %u, %u, %" PRIu64 "s)", straddr,
			stp->root, stp->cost, stp->port, stp->t);
#endif
	clock_gettime(CLOCK_REALTIME, &now);
	lc_peer_t *peer = peer_get_byaddr(relay, AF_INET, src);
	if (peer) {
		if (!peer_stp_msg(relay, peer, stp)) return;
		peer_hello_all(relay); /* root changed, send new hello */
	}
#ifdef DEBUG_STP
	else DEBUG("peer [%s] not found", straddr);
#endif
}

/* deencapsulate data & send on peer TAP device */
static void process_data(lc_relay_t *relay, struct sockaddr_storage *src,
		struct lc_data_pkt_s *pkt, size_t len)
{
	char straddr[INET6_ADDRSTRLEN] = {0};
	struct sockaddr_in6 dst = {0};
	struct iovec iov[2];
	struct msghdr msg = {0};
	lc_peer_t *peer;
	ssize_t byt;

	inet_ntop(src->ss_family, get_in_addr(src), straddr, INET6_ADDRSTRLEN);
	DEBUG("data packet received from [%s] (%zu bytes)", straddr, len);

	peer = peer_get_byaddr(relay, AF_INET, src);
	if (!peer) {
		DEBUG("peer [%s] not found", straddr);
		return;
	}

	/* set source MAC address */
	//memcpy(pkt->eth.h_source, &peer->if_tun.hwaddr, ETH_ALEN * sizeof (uint8_t));

	struct sockaddr_ll sll = {
		.sll_family = AF_PACKET,
		.sll_ifindex = peer->if_tun.ifx,
		.sll_halen = ETH_ALEN,
		.sll_protocol = htons(ETH_P_IPV6)
	};
	memcpy(sll.sll_addr, pkt->eth.h_dest, ETH_ALEN);
	msg.msg_name = &sll;
	msg.msg_namelen = sizeof(struct sockaddr_ll);
	msg.msg_iov = iov;
	msg.msg_iovlen = sizeof iov / sizeof iov[0];
	msg.msg_iov[0].iov_base = &pkt->eth;
	msg.msg_iov[0].iov_len = sizeof(struct ethhdr);
	msg.msg_iov[1].iov_base = pkt->buf;
	msg.msg_iov[1].iov_len = len - sizeof(struct ethhdr) - sizeof(struct lc_stp_s);
	examine_headers(pkt->buf, &dst);
	inet_ntop(AF_INET6, &((struct sockaddr_in6 *)&dst)->sin6_addr, straddr, INET6_ADDRSTRLEN);
	DEBUG("\tWAN: decap packet forwarding to [%s]:%u on %s[%u]", straddr,
			ntohs(dst.sin6_port), peer->if_tun.ifname, peer->if_tun.ifx);

	byt = sendmsg(peer->if_tun.sock, &msg, 0);
	if (byt == -1)
		perror("WAN: sendmsg");
	else if (byt > 0)
		DEBUG("WAN: %zi bytes sent to TUN %s on %s[%u]", byt, straddr,
				peer->if_tun.ifname, peer->if_tun.ifx);
}

static void *thread_relay(void *arg)
{
	lc_relay_t *relay = (lc_relay_t *)arg;
	union lc_peer_pkt pkt = {0};
	struct sockaddr_storage src = {0};
	struct iovec iov[1];
	struct msghdr msg = {0};
	ssize_t byt;
	iov[0].iov_base = &pkt;
	iov[0].iov_len = sizeof pkt;
	msg.msg_name = &src;
	msg.msg_namelen = sizeof src;
	msg.msg_iov = iov;
	msg.msg_iovlen = sizeof iov / sizeof iov[0];
	while (1) {
		byt = recvmsg(relay->if_wan.sock, &msg, 0);
		if (byt == -1) {
			ERROR("recvmsg: %s", strerror(errno));
			continue;
		}
		else if (byt > 0) {
			switch (pkt.type) {
				case PKT_STP:
					process_stp(relay, &src, &pkt.stp);
					break;
				case PKT_DATA:
					process_data(relay, &src, &pkt.data, (size_t)byt);
					break;
				default:
					DEBUG("unknown packet type %i received from peer", pkt.type);
					break;
			}
		}
	}
	return NULL;
}


int lc_relay_start(lc_relay_t *relay)
{
	if ((relay->if_wan.sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("socket(WAN)");
		return -1;
	}
	if (bind_interface(relay->if_wan.sock, AF_INET, relay->if_wan.ifname) == -1) {
		perror("bind_interface");
		return -1;
	}
	pthread_create(&relay->tid[THREAD_TIMER], NULL, &thread_timer, relay);
	pthread_create(&relay->tid[THREAD_RELAY], NULL, &thread_relay, relay);
	return 0;
}

void lc_relay_free(lc_relay_t *relay)
{
	for (int i = 0; i < THREAD_COUNT; i++) {
		if (relay->tid[i]) {
			pthread_cancel(relay->tid[i]);
			pthread_join(relay->tid[i], NULL);
		}
	}
	peer_freelist(relay->peer);
	lc_ctx_free(relay->ctx);
	free(relay);
}
