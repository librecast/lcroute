/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2022 Brett Sheffield <bacs@librecast.net> */

#ifndef _IO_H
#define _IO_H

#include "config.h"
#include <stdio.h>

void io_banner(FILE *out);
int io_prompt(FILE *out, FILE *in);

#endif /* _IO_H */
