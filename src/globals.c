/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2017-2022 Brett Sheffield <bacs@librecast.net> */

#include "globals.h"
#include <sys/param.h>

/* global defaults */

int G_LINE_MAX = 256;
int debug = 0;
mld_t *mld = NULL;
lc_relay_t *rel;
char hostname[HOST_NAME_MAX];
