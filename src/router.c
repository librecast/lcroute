/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2017-2022 Brett Sheffield <bacs@librecast.net> */

#include "globals.h"
#include "io.h"
#include "peer.h"
#include "router.h"
#include "log.h"
#include <arpa/inet.h>
#include <assert.h>
#include <ifaddrs.h>

#ifdef __linux__
#include <linux/if_ether.h>
#include <linux/filter.h>
#include <linux/ipv6.h>
#endif

#include <mld.h>
#include <net/ethernet.h>
#include <net/if.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>
#include <netpacket/packet.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>

#define MLD_FILTER_ENABLED 0
#define HOPLIMIT 64

enum {
	TID_FWD,	/* packet forwarder */
	TID_TUN,	/* tunnel control */
	TID_TIMER,	/* timer events */
	TID_THREADS
};
pthread_t tid[TID_THREADS];

enum {
	ROUTER_IFACE_DENY = 1
};

typedef struct router_if_s {
	char ifname[IFNAMSIZ];
	struct sockaddr hwaddr;    /* MAC address */
	struct sockaddr_in6 llink; /* link-local address */
	int flags;
	int sock;                  /* socket to hold JOINs etc. */
	unsigned int ifx;
} router_if_t;

/* `tcpdump -dd "ip6 multicast && udp"` */
struct sock_filter bpf_ip6multi[] = {
	{ 0x28, 0, 0, 0x0000000c },
	{ 0x15, 0, 8, 0x000086dd },
	{ 0x30, 0, 0, 0x00000026 },
	{ 0x15, 0, 6, 0x000000ff },
	{ 0x30, 0, 0, 0x00000014 },
	{ 0x15, 3, 0, 0x00000011 },
	{ 0x15, 0, 3, 0x0000002c },
	{ 0x30, 0, 0, 0x00000036 },
	{ 0x15, 0, 1, 0x00000011 },
	{ 0x6, 0, 0, 0x00040000 },
	{ 0x6, 0, 0, 0x00000000 },
};
size_t bpf_ip6multilen = sizeof bpf_ip6multi  / sizeof bpf_ip6multi[0];
static int ifxs;                /* number of interfaces */
static router_if_t ifxa[4096];  /* array of active interfaces */
static sem_t sem_ifxa;          /* read/write lock */

static void ifx_add(unsigned int ifx)
{
	struct sockaddr_in6 *llocal;
	struct ifaddrs *ifaddr = NULL;
	struct ifreq ifr = {0};
	DEBUG("(router) adding ifx %u", ifx);
	sem_wait(&sem_ifxa);
	ifxa[ifxs].ifx = ifx;
	if_indextoname(ifx, ifxa[ifxs].ifname);
	if (getifaddrs(&ifaddr)) {
		ERROR("getifaddrs: %s", strerror(errno));
		return;
	}
	ifxa[ifxs].sock = socket(AF_INET6, SOCK_DGRAM, 0);
	strncpy(ifr.ifr_name, ifxa[ifxs].ifname, sizeof ifr.ifr_name);
	if (ioctl(ifxa[ifxs].sock, SIOCGIFHWADDR, &ifr) == -1) {
		perror("ioctl");
	}
	memcpy(&ifxa[ifxs].hwaddr, &ifr.ifr_hwaddr.sa_data, sizeof(struct sockaddr));

	/* find link-local address for binds */
	for (struct ifaddrs *ifa = ifaddr; ifa; ifa = ifa->ifa_next) {
		if (if_nametoindex(ifa->ifa_name) != ifx) continue;
		if (ifa->ifa_addr->sa_family !=AF_INET6) continue;
		llocal = ((struct sockaddr_in6 *)ifa->ifa_addr);
		if (!IN6_IS_ADDR_LINKLOCAL(&llocal->sin6_addr)) continue;
		if (!(ifa->ifa_flags & IFF_MULTICAST)) continue;
		memcpy(&ifxa[ifxs].llink, llocal, sizeof(struct sockaddr_in6));
		break;
	}
	freeifaddrs(ifaddr);
	ifxs++;
	sem_post(&sem_ifxa);
	DEBUG("(router) added ifx %u", ifx);
}

static void ifx_del(unsigned int ifx)
{
	DEBUG("deleting ifx %u", ifx);
	sem_wait(&sem_ifxa);
	for (int i = 0; i < ifxs; i++) {
		if (ifxa[i].ifx == ifx) {
			memmove(&ifxa[i], &ifxa[i+1], sizeof(router_if_t) * (ifxs - i));
			ifxs--;
			break;
		}
	}
	sem_post(&sem_ifxa);
}

static int ifx_nametoindex(const char *ifname)
{
	int idx = -1;
	for (int i = 0; i < ifxs; i++) {
		if (!strcmp(ifxa[i].ifname, ifname)) {
			idx = i;
			break;
		}
	}
	if (idx == -1) errno = ENODEV;
	return idx;
}

router_if_t * router_ifbyname(const char *ifname)
{
	int idx = ifx_nametoindex(ifname);
	if (idx == -1) return NULL;
	return &ifxa[idx];
}

static void ifx_grp_action(unsigned int ifx, struct in6_addr *grp, int action)
{
	struct ipv6_mreq req = {0};
	if (!grp || !grp->s6_addr[0]) return;
	if (!ifx) return;
	req.ipv6mr_interface = ifx;
	memcpy(&(req.ipv6mr_multiaddr), grp, sizeof(struct in6_addr));
	/* propogate JOIN/PART on all other interfaces */
	for (int i = 0; i < ifxs; i++) {
		if (ifxa[i].ifx != ifx) {
			char straddr[INET6_ADDRSTRLEN];
			char *act = (action == IPV6_JOIN_GROUP) ? "JOIN" : "PART";
			inet_ntop(AF_INET6, grp, straddr, INET6_ADDRSTRLEN);
			DEBUG("PIM %s on %s[%u] (%s)", act, ifxa[i].ifname, ifxa[i].ifx, straddr);
			if (setsockopt(ifxa[i].sock, IPPROTO_IPV6, action, &req, sizeof(req)) == -1) {
				if (errno == EADDRINUSE) break;
				ERROR("setsockopt(JOIN/LEAVE GROUP): %s (%s)", strerror(errno), straddr);
			}
			break;
		}
	}
}

static void handle_mld_event(mld_watch_t *watch)
{
	int flags = mld_watch_flags(watch);
	unsigned int ifx = mld_watch_ifx(watch);
	if (flags & MLD_EVENT_JOIN) {
		ifx_grp_action(ifx, mld_watch_grp(watch), IPV6_JOIN_GROUP);
	}
	else if (flags & MLD_EVENT_PART) {
		ifx_grp_action(ifx, mld_watch_grp(watch), IPV6_LEAVE_GROUP);
	}
	else if (flags & MLD_EVENT_IFUP) {
		ifx_add(ifx);
	}
	else if (flags & MLD_EVENT_IFDOWN) {
		ifx_del(ifx);
	}
}

void examine_headers(char *buf, struct sockaddr_in6 *dst)
{
	struct ip6_hdr *ip6h = (struct ip6_hdr *)buf;
	char *ptr = ((char *)ip6h) + (ssize_t)sizeof(struct ip6_hdr);
	struct udphdr *udph = NULL;
	for (uint8_t nxt = ip6h->ip6_nxt; nxt != 255; ) {
		switch (nxt) {
		case 0: /* Hop-by-Hop options header */
			ptr += ((struct ip6_hbh *)ptr)->ip6h_len * 8;
			nxt = ((struct ip6_hbh *)ptr)->ip6h_nxt;
			break;
		case 17: /* UDP */
			udph = (struct udphdr *)ptr;
			nxt = 255;
			break;
		case 58: /* ICMPv6 */
			nxt = 255;
			break;
		default:
			nxt = 255;
			break;
		}
	}
	if (dst) memcpy(&dst->sin6_addr, &ip6h->ip6_dst, sizeof(struct in6_addr));
	if (udph && dst) dst->sin6_port = udph->uh_dport;
}

static void *thread_fwd(void *arg)
{
	char buf[65535] = {0};
	struct ethhdr eth = {0};
	struct ipv6hdr *ipv6;
	struct msghdr msg = {0};
	struct sockaddr_ll src = {0};
	struct sockaddr_in6 dst = {0};
	struct iovec iov[2];
	struct sockaddr_ll sll = {
		.sll_family = AF_PACKET,
		.sll_halen = ETH_ALEN,
		.sll_protocol = htons(ETH_P_IPV6)
	};
	struct sock_fprog bpf = {
		.len = bpf_ip6multilen,
		.filter = bpf_ip6multi,
	};
	ssize_t byt;
	unsigned int ifx;
	int sock;

	DEBUG("forwarder thread started");

	iov[0].iov_base = &eth;
	iov[0].iov_len = sizeof eth;
	iov[1].iov_base = buf;
	sock = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	if (setsockopt(sock, SOL_SOCKET, SO_ATTACH_FILTER, &bpf, sizeof(bpf)) == -1) {
		ERROR("unable to set BPF filter");
		return NULL;
	}
	while (1) {
		msg.msg_name = &src;
		msg.msg_namelen = sizeof(struct sockaddr_storage);
		msg.msg_iov = iov;
		msg.msg_iovlen = sizeof iov / sizeof iov[0];
		iov[1].iov_len = sizeof buf;
		byt = recvmsg(sock, &msg, 0);
		if (byt == -1) {
			ERROR("recvmsg: %s", strerror(errno));
		}
		else if (byt > 0) {
			char ifname[IFNAMSIZ];
			ifx = (unsigned int)src.sll_ifindex;
			if_indextoname(src.sll_ifindex, ifname);
			ipv6 = (struct ipv6hdr *)buf;
			DEBUG("(router) %zi bytes received on %s[%u], hlim = %u", byt, ifname, ifx,
					ipv6->hop_limit);

			if (ipv6->hop_limit > 1) {
				ipv6->hop_limit--;
				if (ipv6->hop_limit > HOPLIMIT) ipv6->hop_limit = HOPLIMIT;
				/* forward on all other ifaces */
				sem_wait(&sem_ifxa);

				int idx = ifx_nametoindex(ifname);
				if (ifxa[idx].flags & ROUTER_IFACE_DENY) {
					DEBUG("ingress iface %s[%u] denied",
							ifxa[idx].ifname, ifxa[idx].ifx);
					continue;
				}

				for (int i = 0; i < ifxs; i++) {
					if (ifx == ifxa[i].ifx) continue;
					if (ifxa[i].flags & ROUTER_IFACE_DENY) {
						DEBUG("egress iface %s[%u] denied",
								ifxa[i].ifname, ifxa[i].ifx);
						continue;
					}
					examine_headers(buf, &dst);
#if MLD_FILTER_ENABLED
					if (!mld_filter_grp_cmp(mld, ifxa[i].ifx, &dst.sin6_addr)) continue;
#endif
					sll.sll_ifindex = ifxa[i].ifx,
					memcpy(sll.sll_addr, eth.h_dest, ETH_ALEN);
					msg.msg_name = &sll;
					msg.msg_namelen = sizeof(struct sockaddr_ll);
					msg.msg_iov[1].iov_len = byt - sizeof eth;
					DEBUG("(router) forwarding on %s[%u]", ifxa[i].ifname, ifxa[i].ifx);
					if ((byt = sendmsg(sock, &msg, 0)) == -1) {
						ERROR("sendmsg: %s", strerror(errno));
					}
				}
				sem_post(&sem_ifxa);
			}
		}
	}
	return arg;
}

int router_iface_allow(char *ifname)
{
	int idx = ifx_nametoindex(ifname);
	if (idx == -1) return -1;
	//sem_wait(&sem_ifxa);
	if (ifxa[idx].flags & ROUTER_IFACE_DENY)
		ifxa[idx].flags ^= ROUTER_IFACE_DENY;
	//sem_post(&sem_ifxa);
	return 0;
}

int router_iface_deny(char *ifname)
{
	int idx = ifx_nametoindex(ifname);
	if (idx == -1) return -1;
	//sem_wait(&sem_ifxa);
	ifxa[idx].flags |= ROUTER_IFACE_DENY;
	//sem_post(&sem_ifxa);
	return 0;
}

void router_iface_list(FILE *f)
{
	for (int i = 0; i < ifxs; i++) {
		fprintf(f, "%s[%u]\n", ifxa[i].ifname, ifxa[i].ifx);
	}
}

void router_start(void)
{
	sem_init(&sem_ifxa, 0, 1);
	pthread_create(&tid[TID_FWD], NULL, &thread_fwd, NULL);
	if (debug) mld_loglevel_set(LOG_LOGLEVEL_DEFAULT);
	mld = mld_init(0);
	mld_watch_add(mld, 0, NULL, &handle_mld_event, NULL, MLD_EVENT_ALL);
	mld_start(mld);
}

void router_stop(void)
{
	for (int i = 0; i < TID_THREADS; i++) {
		if (tid[i]) {
			pthread_cancel(tid[i]);
			pthread_join(tid[i], NULL);
			tid[i] = 0;
		}
	}
	if (mld) {
		mld_stop(mld);
		mld_free(mld);
		mld = NULL;
	}
	sem_destroy(&sem_ifxa);
}
