/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2017-2022 Brett Sheffield <brett@librecast.net> */

#ifndef _PEER_PVT_H
#define _PEER_PVT_H 1

#include "peer.h"
#include "globals.h"
#include "log.h"
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <pthread.h>
#include <librecast.h>
#include <librecast/if.h>
#include <unistd.h>

#ifdef __linux__
#include <linux/if_ether.h>
#endif

#define RELAY_PEER_INTERVAL 2 /* how often (s) to send hello msgs */
#define RELAY_PEER_EXPIRES  20 /* how long (s) before peer is considered down */
#define RELAY_PORT_UDP 2268
#define IPV6_BYTES 16

enum {
	THREAD_RELAY,
	THREAD_TIMER,
	THREAD_COUNT
};

enum {
	PEER_UP   = 1, /* peer is up (apparently) */
	PEER_DR   = 2, /* we are designated router for link */
	PEER_ROOT = 4, /* peer is path to root */
};

enum {
	PKT_STP  = 0,
	PKT_DATA = 1
};

struct udp6hdr {
	struct in6_addr src;
	struct in6_addr dst;
	uint32_t udplen;
	uint8_t zero[3];
	uint8_t nxt;
	uint16_t sport;
	uint16_t dport;
	uint16_t len;
	uint16_t csum;
	char data[];
};

struct lc_interface_s {
	char ifname[IFNAMSIZ];
	struct sockaddr hwaddr;
	unsigned int ifx;
	int sock;                      /* socket, yeah (satellite of love) */
};

struct lc_peer_s {
	lc_peer_t                *next;
	lc_relay_t		*relay;
	struct sockaddr_storage  saddr;
	struct timespec           seen; /* last seen */
	struct lc_interface_s   if_tun; /* tunnel interface */
	struct lc_stp_s            stp; /* last STP msg from peer */
	pthread_t                  tid; /* LAN listener thread for peer */
	int                      tapfd; /* tap device file descriptor */
	int                         af; /* address family AF_INET | AF_INET6 */
	int                      flags;
	int                         id; /* peer ID */
};

struct lc_relay_s {
	lc_ctx_t *ctx;
	struct lc_interface_s if_wan; /* WAN interface */
	lc_peer_t              *peer;
	pthread_t  tid[THREAD_COUNT];
	struct lc_stp_s          stp; /* current STP config */
	uint64_t                  id; /* relay ID */
};

struct lc_data_pkt_s {
	struct lc_stp_s     stp;
	struct ethhdr       eth;
	char                buf[1500];
};

union lc_peer_pkt {
	uint8_t              type;
	struct lc_stp_s       stp;
	struct lc_data_pkt_s data;
};

enum {
	TUN,
	WAN
};

#endif /* _PEER_PVT_H */
