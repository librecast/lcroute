/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2017-2022 Brett Sheffield <bacs@librecast.net> */

#ifndef _GLOBALS_H
#define _GLOBALS_H 1

#define DEBUG_ON 1
#include <mld.h>
#include "peer.h"

extern int G_LINE_MAX;
extern int debug;
extern mld_t *mld;
extern lc_relay_t *rel;
extern char hostname[];

#endif
