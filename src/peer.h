/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2017-2022 Brett Sheffield <brett@librecast.net> */

#ifndef _PEER_H
#define _PEER_H 1

#include <sys/types.h>
#include <stdint.h>

typedef struct lc_interface_s lc_interface_t;
typedef struct lc_peer_s lc_peer_t;
typedef struct lc_relay_s lc_relay_t;

struct lc_stp_s {
	uint8_t   type;
	uint8_t   cost;
	uint16_t  port;
	uint64_t  root;
	uint64_t     t; /* timestamp set by root */
};

/* add a peer by address
 *  - af
 *	AF_INET | AF_INET6
 *  - addr
 *	character string containing IP address
 * returns peer ID >= 0 on success, -1 on error */
int peer_add(lc_relay_t *relay, int af, const char *restrict addr);

/* get peer by id */
lc_peer_t *peer_get(lc_relay_t *relay, int peerid);

/* return ID of peer */
int peer_id(lc_peer_t *peer);

void peer_list(lc_relay_t *relay);

struct timespec *lc_peer_lastseen(lc_relay_t *relay, int peerid);

void peer_freelist(lc_peer_t *peerlist);

/* get/set relay ID */
uint64_t lc_relay_id_get(lc_relay_t *relay);
void lc_relay_id_set(lc_relay_t *relay, uint64_t id);

lc_relay_t *lc_relay_init(const char *restrict ifname);

/* start relay on interface ifname */
int lc_relay_start(lc_relay_t *relay);

void lc_relay_free(lc_relay_t *relay);

void calculate_root(lc_relay_t *relay);
void peer_check(lc_relay_t *relay, lc_peer_t *peer);
int peer_stp_msg(lc_relay_t *relay, lc_peer_t *peer, const struct lc_stp_s *stp);
int lc_stp_cmp(const struct lc_stp_s *stp1, const struct lc_stp_s *stp2);

#endif /* _PEER_H */
