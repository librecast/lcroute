/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2022 Brett Sheffield <bacs@librecast.net> */

#ifndef _ROUTER_H
#define _ROUTER_H 1

#include <linux/filter.h>

extern struct sock_filter bpf_ip6multi[];
extern size_t bpf_ip6multilen;

void router_start(void);
void router_stop(void);
void router_iface_list(FILE *f);
int router_iface_allow(char *ifname);
int router_iface_deny(char *ifname);
void examine_headers(char *buf, struct sockaddr_in6 *dst);

#endif /* _ROUTER_H */
