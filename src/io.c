/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2022 Brett Sheffield <bacs@librecast.net> */

#include "io.h"
#include "globals.h"
#include "log.h"
#include "router.h"
#include <mld.h>
#include <stdlib.h>
#include <string.h>

/* TODO replace this mess with flex + bison */
static int io_parse_command(FILE *out, FILE *in, char *s, size_t len)
{
	char *tok;
	tok = strtok(s, " ");
	if (!tok) return 0;
	if (!strcmp(tok, "help")) {
		fputs("HELP\n", out);
		fputs("no help here\n", out);
	}
	else if (!strcmp(tok, "debug")) {
		fputs("DEBUG\n", out);
		debug = 0;
		mld_loglevel_set(127);
	}
	else if (!strcmp(tok, "nodebug")) {
		fputs("NODEBUG\n", out);
		debug = -1;
		mld_loglevel_set(LOG_LOGLEVEL_DEFAULT);
	}
	else if (!strcmp(tok, "mld")) {
		tok = strtok(NULL, " ");
		if (tok && !strcmp(tok, "start")) {
			fputs("MLD START\n", out);
			mld = mld_init(0);
			mld_start(mld);
		}
		else if (tok && !strcmp(tok, "stop")) {
			fputs("MLD STOP\n", out);
			router_stop();
			DEBUG("mld stopped");
		}
		else {
			fputs("syntax error\n", out);
		}
	}
	else if (!strcmp(tok, "iface")) {
		char *act = strtok(NULL, " ");
		if (!act) return 0;
		if (!strcmp(act, "list")) {
			fprintf(out, "IFACE LIST\n");
			router_iface_list(out);
			return 0;
		}
		else if (!strcmp(act, "allow")) {
			tok = strtok(NULL, " ");
			fprintf(out, "IFACE ALLOW %s\n", tok);
			if (router_iface_allow(tok) == -1) {
				fprintf(out, "%s\n", strerror(errno));
			}
		}
		else if (!strcmp(act, "deny")) {
			tok = strtok(NULL, " ");
			fprintf(out, "IFACE DENY %s\n", tok);
			if (router_iface_deny(tok) == -1) {
				fprintf(out, "%s\n", strerror(errno));
			}
		}
	}
	else if (!strcmp(tok, "peer")) {
		if (!rel) return 0;
		char *act = strtok(NULL, " ");
		if (!act) return 0;
		if (!strcmp(act, "list")) {
			fprintf(out, "PEER LIST\n");
			peer_list(rel);
			return 0;
		}
		tok = strtok(NULL, " ");
		if (!tok) return 0;
		if (!strcmp(act, "add")) {
			if (peer_add(rel, AF_INET, tok) == -1) return 0;
			fprintf(out, "PEER ADD %s\n", tok);
		}
		else if (!strcmp(act, "del")) {
			fprintf(out, "PEER DEL %s\n", tok);
			//peer_del(rel, AF_INET, tok);
		}

	}
	else if (!strcmp(tok, "relay")) {
		char *act = strtok(NULL, " ");
		if (!act) return 0;
		if (!strcmp(act, "start")) {
			tok = strtok(NULL, " ");
			if (!tok) return 0;
			if ((rel = lc_relay_init(tok)) == NULL) return -1;
			if (lc_relay_start(rel) == -1) return 0;
			fprintf(out, "RELAY START %s\n", tok);
		}
		else if (!strcmp(act, "stop")) {
			lc_relay_free(rel);
			rel = NULL;
			fprintf(out, "RELAY STOP\n");
		}
		else if (!strcmp(act, "id")) {
			if (!rel) return 0;
			uint64_t id;
			tok = strtok(NULL, " ");
			if (tok) {
				id = atoll(tok);
				if (!id) {
					fprintf(stderr, "invalid id '%s'\n", tok);
					return 0;
				}
				lc_relay_id_set(rel, id);
			}
			else {
				id = lc_relay_id_get(rel);
			}
			fprintf(out, "RELAY ID %" PRIu64 "\n", id);
		}
	}
	else if (!strcmp(tok, "quit")) {
		fputs("QUIT\n", out);
		return 1;
	}
	else {
		fputs("I don't understand\n", out);
	}
	return 0;
}

void io_banner(FILE *out)
{
	fprintf(out, "%s v%s\n", PACKAGE_NAME, PACKAGE_VERSION);
}

int io_prompt(FILE *out, FILE *in)
{
	char buf[G_LINE_MAX];
	size_t len;
	fprintf(out, "%s> ", PACKAGE_NAME);
	fflush(out);
	if (!fgets(buf, sizeof buf, in)) return -1;
	len = strlen(buf) - 1;
	buf[len] = '\0'; len--;
	return io_parse_command(out, in, buf, strlen(buf));
}
